#!/usr/bin/env python
from __future__ import print_function
from __future__ import unicode_literals

import sys
import argparse
import re

def parse_input_file(testfile):
	# Regexs for getting data from file
	answer_regex = re.compile('^ANSWER:\\s*/(.*)/$')
	test_regex = re.compile('^(m|n) (.*)$')

	# Read in and break lines up
	parsed = {'answer': None, 'tests': {}}
	with open(testfile, 'r') as f:
		for line in f.readlines():
			# If the line isn't an answer regex or a test line, ignore
			mAnswer = answer_regex.match(line)
			if mAnswer is not None:
				parsed['answer'] = mAnswer.group(1)
			else:
				mTest = test_regex.match(line)
				if mTest is not None:
					test_is_match = (mTest.group(1) == 'm')
					parsed['tests'][mTest.group(2)] = test_is_match
	
	return parsed

def print_matches(parsed_file):
	print('Test cases that SHOULD be matched:')
	for test, matches in parsed_file['tests'].iteritems():
		if matches:
			print(test)

def print_nonmatches(parsed_file):
	print('Test cases that should NOT be matched:')
	for test, matches in parsed_file['tests'].iteritems():
		if not matches:
			print(test)

def run_test(parsed_file, override_regex=None, verbose=False):
	# Get ready to test
	if override_regex is not None:
		reg = re.compile(override_regex, flags=re.IGNORECASE)
	else:
		reg = re.compile(parsed_file['answer'], flags=re.IGNORECASE)

	# Status
	print('Testing regex /{}/, {} characters long'.format(reg.pattern, len(reg.pattern)))

	# Assume we're passing for now, then compare to each line in the file
	passing = True
	for test, expected in parsed_file['tests'].iteritems():
		m = reg.search(test)

		if m is not None and not expected:
			print('Bad: Matched {} (captured \'{}\')'.format(test, m.group(0)))
			passing = False
		elif verbose and m is not None and expected:
			print('Good: Matched {} (captured \'{}\')'.format(test, m.group(0)))
		elif verbose and m is None and not expected:
			print('Good: No match for {}'.format(test))
		elif m is None and expected:
			print('Bad: No match for {}'.format(test))
			passing = False
	
	# Report
	if passing:
		print('All tests passed!')
	else:
		print('Answer failed')

	return passing

def main(argv):
	parser = argparse.ArgumentParser(description='Test a regex against a file')
	parser.add_argument('--matches', '-m', action='store_true', help='Print only strings that should be matched in the input file')
	parser.add_argument('--non-matches', '-n', action='store_true', help='Print only strings that should NOT be matched in the input file')
	parser.add_argument('testfile', help='Regex Golf input file')
	parser.add_argument('--regex', '-r', help='Regex to match against input file, overrides file regex')
	parser.add_argument('--verbose', '-v', action='store_true', help='Display results for every test case, including what portion matched')
	args = parser.parse_args()

	# Parse it out
	parsed_file = parse_input_file(args.testfile)

	if args.matches:
		# Print only matches from this file
		print_matches(parsed_file)
	elif args.non_matches:
		# Print only non-matches from this file
		print_nonmatches(parsed_file)
	else:
		# Run test
		run_test(parsed_file, override_regex=args.regex, verbose=args.verbose)


if __name__ == '__main__':
	sys.exit(main(sys.argv))

